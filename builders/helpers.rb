def matrix(h, w)
  Array.new(h) { |i| Array.new(w) { |j|
    if block_given? # block is given
      yield(i, j) # no need to use call to execute the block
    else
      nil
    end
  } }
end

class Dimensions
  def initialize(dims)
    @shape = dims
  end

  def check_bounds(*indices)
    if indices.size != @shape.size
      raise "dimensions mismatch given #{indices.size} index (indices) for a #{@shape.size} dimension(s) matrix"
    end

    i = 0
    while i < @shape.size
      if indices[i] < 0 or indices[i] >= @shape[i]
        raise "index out of bounds #{indices[i]} out of #{@shape[i]} (dimension #{i})."
      end
      i += 1
    end
  end

  def decompose(idx)
    tmp = idx
    indices = Array.new(@shape.size)

    i = @shape.size - 1
    while i >= 0
      dim = @shape[i]
      indices[i] = tmp % dim
      tmp = (tmp - indices[i]) / dim
      i -= 1
    end
    indices
  end

  def compose(*indices)
    check_bounds(*indices)
    index = indices[0]
    if indices[0] >= @shape[0]
      raise
    end
    for i in 1...indices.size
      index *= @shape[i]
      index += indices[i]
    end
    index
  end
end

class NDArray

  def initialize(dims)
    @shape = Dimensions.new(dims)
    @shape.freeze
    if block_given?
      @values = Array.new(dims.inject(1, :*)) { |i| yield *@shape.decompose(i) }
    else
      @values = Array.new(dims.inject(1, :*))
    end
  end

  def ndims
    @shape.size
  end

  def [](*indices)
    @values[@shape.compose(*indices)]
  end

  def []=(*indices, value)
    @values[@shape.compose(*indices)] = value
  end
end