#!/usr/bin/env ruby

require 'slop'
require 'common'
require 'specification'

require_relative '../builders/cipher_dag_builder'
require_relative '../builders/helpers'

def process_options(arg)
  begin
    opts = Slop.parse(arg) do |o|
      o.int '-r', '--round', 'number of rounds', default: 41
      o.on '--help', 'print the help' do
        puts o
        exit
      end
    end
  rescue Slop::Error => e
    warn e
    exit(2)
  end
  opts
end

BR = 32
BR_HALF = 16

WARP_SBOX_LOOKUP_TABLE = [
  0xc, 0xa, 0xd, 0x3, 0xe, 0xb, 0xf, 0x7, 0x8, 0x9, 0x1, 0x5, 0x0, 0x2, 0x4, 0x6
]

PI = [
  31, 6, 29, 14, 1, 12, 21, 8, 27, 2, 3, 0, 25, 4, 23, 10,
  15, 22, 13, 30, 17, 28, 5, 24, 11, 18, 19, 16, 9, 20, 7, 26
]

RC0 = [
  0x0, 0x0, 0x0, 0x1, 0x3, 0x7, 0xf, 0xf, 0xf, 0xe, 0xd, 0xa, 0x5, 0xa, 0x5, 0xb, 0x6, 0xc, 0x9, 0x3, 0x6,
  0xd, 0xb, 0x7, 0xe, 0xd, 0xb, 0x6, 0xd, 0xa, 0x4, 0x9, 0x2, 0x4, 0x9, 0x3, 0x7, 0xe, 0xc, 0x8, 0x1, 0x2
]

RC1 = [
  0x0, 0x4, 0xc, 0xc, 0xc, 0xc, 0xc, 0x8, 0x4, 0x8, 0x4, 0x8, 0x4, 0xc, 0x8, 0x0, 0x4, 0xc, 0x8, 0x4, 0xc,
  0xc, 0x8, 0x4, 0xc, 0x8, 0x4, 0x8, 0x0, 0x4, 0x8, 0x0, 0x4, 0xc, 0xc, 0x8, 0x0, 0x0, 0x4, 0x8, 0x4, 0xc
]

NIBBLE = HalfOpenRange.new(min: 0, max: 16)

class WarpConfig
  def initialize(options)
    @nr = options["round"]
    @builder = CipherDagBuilder.new
    @xor2 = @builder.declare_linear_function([NIBBLE, NIBBLE], [NIBBLE], Specification::BitwiseXOR.new)
    @xor3 = @builder.declare_linear_function([NIBBLE, NIBBLE, NIBBLE], [NIBBLE], Specification::BitwiseXOR.new)
    @s = @builder.declare_sbox_function([NIBBLE], [NIBBLE], WARP_SBOX_LOOKUP_TABLE)
  end

  private
  def permutation(x)
    xp = x[0..-1] # Array copy
    for i in 0...BR
      x[PI[i]] = xp[i]
    end
  end

  def encryption(x, mk, nr)
    k = mk.each_slice(16).to_a
    @builder.named("K_0", k[0])
    @builder.named("K_1", k[1])
    for r in 1...nr
      for i in 0...BR_HALF
        x[2 * i + 1] = @xor3.call(@s.call(x[2 * i]), k[(r - 1) % 2][i], x[2 * i + 1])
      end
      x[1] = @xor2.call(x[1], RC0[r])
      x[3] = @xor2.call(x[3], RC1[r])
      permutation(x)
      @builder.named("X_#{r}", x)
    end
    for i in 0...BR_HALF
      x[2 * i + 1] = @xor3.call(@s.call(x[2 * i]), k[(nr - 1) % 2][i], x[2 * i + 1])
    end
    x[1] = @xor2.call(x[1], RC0[nr])
    x[3] = @xor2.call(x[3], RC1[nr])
    x
  end

  public
  def build
    plaintext = @builder.plaintext(BR, NIBBLE)
    key = @builder.key(BR, NIBBLE)
    @builder.ciphertext(encryption(plaintext, key, @nr))
    @builder.build
  end

end

# usage: warp.rb --round N
# generates a warp cipher with N rounds.
def main
  options = process_options(ARGV)
  puts WarpConfig.new(options).build.to_json
end

main
