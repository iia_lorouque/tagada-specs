#!/usr/bin/env ruby

require 'slop'
require 'common'
require 'specification'
require_relative '../builders/cipher_dag_builder'
require_relative '../builders/helpers'

def process_options(arg)
  begin
    opts = Slop.parse(arg) do |o|
      o.int '-v', '--version', 'key size ({80, 128})'
      o.int '-r', '--round', 'number of rounds', default: nil
      o.on '--help', 'print the help' do
        puts o
        exit
      end
    end
  rescue Slop::Error => e
    warn e
    exit(2)
  end
  opts
end

S = [
  0x0c, 0x00, 0x0f, 0x0a,
  0x02, 0x0b, 0x09, 0x05,
  0x08, 0x03, 0x0d, 0x07,
  0x01, 0x0e, 0x06, 0x04
]

PI = [
  0x05, 0x00, 0x01, 0x04,
  0x07, 0x0c, 0x03, 0x08,
  0x0d, 0x06, 0x09, 0x02,
  0x0f, 0x0a, 0x0b, 0x0e
]

CON = [
  nil, 0x01, 0x02, 0x04, 0x08,
  0x10, 0x20, 0x03, 0x06,
  0x0c, 0x18, 0x30, 0x23,
  0x05, 0x0a, 0x14, 0x28,
  0x13, 0x26, 0x0f, 0x1e,
  0x3c, 0x3b, 0x35, 0x29,
  0x11, 0x22, 0x07, 0x0e,
  0x1c, 0x38, 0x33, 0x25,
  0x09, 0x12, 0x24
]

NIBBLE = HalfOpenRange.new(min: 0, max: 16)
NILLBES_BY_INT = 4

class TwineConfig
  def initialize(options)
    @version = options['version']
    unless @version == 80 || @version == 128
      raise "invalid cipher version. Should be either 80 or 128, given #{@version}"
    end
    @nr = options['round'] || 36
    @builder = CipherDagBuilder.new
    @sb = @builder.declare_sbox_function([NIBBLE], [NIBBLE], S)
    @xor2 = @builder.declare_linear_function([NIBBLE, NIBBLE], [NIBBLE], Specification::BitwiseXOR.new)
  end

  private

  def named(name, iter)
    @builder.named(name, iter)
  end

  def rot4(wk)
    old = Array.new(wk)
    for i in 0..3
      wk[i] = old[(i + 1) % 4]
    end
    wk
  end

  def rot16_80bits(wk)
    old = Array.new(wk)
    for i in 0..19
      wk[i] = old[(i + 4) % 20]
    end
    wk
  end

  def rot16_128bits(wk)
    old = Array.new(wk)
    for i in 0..31
      wk[i] = old[(i + 4) % 32]
    end
    wk
  end

  def key_schedule_80bits(k)
    wk = k
    rk = Array.new(@nr + 1)
    for r in 1..@nr - 1
      rk[r] = named("RK_#{r}", [wk[1], wk[3], wk[4], wk[6], wk[13], wk[14], wk[15], wk[16]])
      wk[1] = @xor2.call(wk[1], @sb.call(wk[0]))
      wk[4] = @xor2.call(wk[4], @sb.call(wk[16]))
      wk[7] = @xor2.call(wk[7], CON[r] >> 3)
      wk[19] = @xor2.call(wk[19], CON[r] & 0b111)
      wk = rot4(wk)
      wk = rot16_80bits(wk)
    end
    rk[@nr] = named("RK_#{@nr}", [wk[1], wk[3], wk[4], wk[6], wk[13], wk[14], wk[15], wk[16]])
    rk
  end

  def key_schedule_128bits(k)
    wk = k
    rk = Array.new(@nr + 1)
    for r in 1..@nr - 1
      rk[r] = named("RK_#{r}", [wk[2], wk[3], wk[12], wk[15], wk[17], wk[18], wk[28], wk[31]])
      wk[1] = @xor2.call(wk[1], @sb.call(wk[0]))
      wk[4] = @xor2.call(wk[4], @sb.call(wk[16]))
      wk[23] = @xor2.call(wk[23], @sb.call(wk[30]))
      wk[7] = @xor2.call(wk[7], CON[r] >> 3)
      wk[19] = @xor2.call(wk[19], CON[r] & 0b111)
      wk = rot4(wk)
      wk = rot16_128bits(wk)
    end
    rk[@nr] = named("RK_#{@nr}" ,[wk[2], wk[3], wk[12], wk[15], wk[17], wk[18], wk[28], wk[31]])
    rk
  end

  def encryption(plaintext, key)
    x = plaintext
    if @version == 80
      rk = key_schedule_80bits(key)
    else
      rk = key_schedule_128bits(key)
    end
    for i in 1..@nr - 1
      for j in 0..7
        x[2 * j].alias("X_#{i}_#{j}")
        x[2 * j + 1] = @xor2.call(@sb.call(@xor2.call(x[2 * j], rk[i][j])), x[2 * j + 1])
      end
      x_i = Array.new(x)
      for h in 0..15
        x[PI[h]] = x_i[h]
      end
      named("X_#{i}", x)  
    end
    for j in 0..7
      x[2 * j + 1] = @xor2.call(@sb.call(@xor2.call(x[2 * j], rk[@nr][j])), x[2 * j + 1])
    end
    named("X_#{@nr}", x)
    x
  end

  public

  def build
    plaintext = @builder.plaintext(16, NIBBLE)
    if @version == 80
      key = @builder.key(20, NIBBLE)
    else
      key = @builder.key(32, NIBBLE)
    end
    @builder.ciphertext(encryption(plaintext, key))
    @builder.build
  end

end

# usage: ciphers/twine.rb -v 80 -r 3
def main
  options = process_options(ARGV)
  twine = TwineConfig.new(options)
  puts twine.build.to_json
end

main
